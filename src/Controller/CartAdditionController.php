<?php

/**
 * @file
 * Contains \Drupal\add_cart_by_url\Controller\CartAdditionController.
 */

namespace Drupal\add_cart_by_url\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation;

class CartAdditionController extends ControllerBase {
  public function process_product($product_id) { 
    // Get Current User
    $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());

    // Load Order Type
    // @TO DO: Get Dynamic Order Type
    // $order_type = \Drupal\commerce_order\Entity\OrderType::load('default');

    // @TO DO: Allow for Selection of what type of products have dynamic URLS in Admin Settings
    // $order_item_type = \Drupal\commerce_order\Entity\OrderItemType::load('default');

    // Load Product
    // @TO DO: Allow option for URL to be determined at the product level
    // $product = \Drupal\commerce_product\Entity\Product::load(1);
       $product = \Drupal\commerce_order\Entity\OrderItem::load($product_id);

    // Create the billing profile.
    $profile = \Drupal\profile\Entity\Profile::create([
      'type' => 'customer',
      'uid' => \Drupal::currentUser()->id(),
    ]);

    $profile->save();

    // Set Order Item
    $order_item = \Drupal\commerce_order\Entity\OrderItem::create([
      'type' => 'default',
      'purchased_entity' => $product_id,
      'quantity' => 1,
      'unit_price' => NULL,
    ]);

    switch ($product_sku) {
      case 1:
        $unit_price = new \Drupal\commerce_price\Price('299.99', 'USD');
        $order_item->setUnitPrice($unit_price);
      break;

      case 2:
        $unit_price = new \Drupal\commerce_price\Price('599.99', 'USD');
        $order_item->setUnitPrice($unit_price);
      break;

      case 3:
        $unit_price = new \Drupal\commerce_price\Price('299.99', 'USD');
        $order_item->setUnitPrice($unit_price);
      break;

      case 4:
        $unit_price = new \Drupal\commerce_price\Price('2999.99', 'USD');
        $order_item->setUnitPrice($unit_price);
      break;
    }

    $order_item->save();

    // Create the User's Order, Order Items are Products
    $order = \Drupal\commerce_order\Entity\Order::create([
      'type' => 'default',
      'store_id' => 1,
      'uid' => \Drupal::currentUser()->id(),
      'mail' => 'user@example.com',
      'ip_address' =>  \Drupal::request()->getClientIp(),
      'billing_profile' => $profile,
      'state' => 'draft',
      'placed' => time(),
      'order_items' => [$order_item],
    ]);

    // Save the Order
    $order->save();

    return $this->redirect('commerce_checkout.form', ['commerce_order' => $order->getOrderNumber()]); 
  }
}